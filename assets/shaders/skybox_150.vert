#version 150

in vec3 position;
out vec3 tex_coord;

uniform mat4 perspective;
uniform mat4 view;

void main() {
  gl_Position = (perspective*view*vec4(position, 1.0)).xyww;
  tex_coord = position;
}
