
#version 150

in vec3 tex_coord;
out vec4 color;
uniform samplerCube cubemap;

void main() {
  color = texture(cubemap, tex_coord*vec3(-1.0,1.0,1.0));
}
