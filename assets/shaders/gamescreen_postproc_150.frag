#version 150 core

uniform sampler2D tex;
uniform vec2 resolution;

in vec2 v_tex_coords;

out vec4 color;

const float vignette_radius = 0.75;
const float vignette_inner_radius = 0.3;

void main() {
  color = texture(tex, v_tex_coords)*vec4(vec3(smoothstep(vignette_radius, vignette_inner_radius, distance(gl_FragCoord.xy/resolution, vec2(0.5,0.5)))), 1.0);
}
