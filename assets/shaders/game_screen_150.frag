#version 150 core

uniform sampler2D tex;

in vec2 v_tex_coords;

out vec4 color;

void main() {
  vec4 texcolor = texture(tex, v_tex_coords);
  
  color = texcolor;
  if (color.a < 0.5) {
    discard;
  };
}
