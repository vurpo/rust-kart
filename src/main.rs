#![feature(custom_derive, plugin)]
#![plugin(serde_macros)]
#[macro_use]
extern crate glium;
extern crate nalgebra;
extern crate image;
extern crate sdl2;
extern crate time;
extern crate core;
extern crate tiled;
extern crate serde_macros;
extern crate serde_json;
extern crate num_traits;

pub mod game;
pub mod render;
pub mod math;

use std::cell::RefCell;
use std::rc::Rc;

use game::{
  GameScreen,
  Screen,
  InputProcessor,
};

use time::PreciseTime;

fn main() {
  use glium::{
    DisplayBuild,
  };


  /*let sdl2_context = sdl2::init().unwrap();
  let controller_subsystem = sdl2_context.game_controller().unwrap();
  let mut event_pump = sdl2_context.event_pump().unwrap();
  
  controller_subsystem.set_event_state(true);
  let mut controllers: Vec<sdl2::controller::GameController> = Vec::new();
  println!("{} controllers connected", controller_subsystem.num_joysticks().unwrap());
  for n in 0..controller_subsystem.num_joysticks().unwrap() {
    println!("Controller {}: {}", n, controller_subsystem.name_for_index(n).unwrap());
  }

  let mut ds4 = controller_subsystem.open(0).unwrap();*/
  
  let start = PreciseTime::now();
  println!("DEBUG: creating window");

  let display = glium::glutin::WindowBuilder::new().with_dimensions(500,500).with_depth_buffer(24).build_glium().unwrap();

  let mut game_screen = RefCell::new(GameScreen::new(&display, "testlevel".to_string()));

  let current_screen: &RefCell<Screen>;
  current_screen = &game_screen;

  let input_processor: &RefCell<InputProcessor>;
  input_processor = &game_screen;

  let mut last: PreciseTime = PreciseTime::now();
  let mut now: PreciseTime;

  println!("DEBUG: starting main loop");

  'mainloop: loop {

    now = PreciseTime::now();
    let frame_time: f64 = last.to(now).num_milliseconds() as f64 / 1000.0;
    last = now;

    for event in display.poll_events() {
      match event {
        glium::glutin::Event::Closed => break 'mainloop,
        
        glium::glutin::Event::KeyboardInput(_,_,keycode) => {
          if let Some(keycode) = keycode {
            if keycode == glium::glutin::VirtualKeyCode::Escape {
              break 'mainloop;
            }
          }
          input_processor.borrow_mut().input_event(event);
        }

        _ => (),
      }
    }
    { 
      let mut current_screen = current_screen.borrow_mut();

      current_screen.update(frame_time);

      let mut target = display.draw();

      current_screen.render(&mut target);

      target.finish().unwrap();
    }

    /*controller_subsystem.update();

    use sdl2::event::Event;
    for event in event_pump.poll_iter() {
      println!("{:?}", event);
    }*/
  }
}
