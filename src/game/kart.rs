use std::rc::Rc;
use std::fs::File;
use std::io::Read;
use nalgebra::{
  Point2,
  Vector2,
  Vector3,
  UnitQuaternion,
  Rotation,
};
use serde_json::{
  from_str,
};
use render::{
  Decal,
  Transform,
  load_srgb_texture,
  SrgbTextureRegion,
};
use glium;

pub struct KartTransform {
  pub position: Point2<f64>,
  pub rotation: f64,
  pub velocity: Vector2<f64>,
}

#[derive(Serialize,Deserialize)]
#[allow(non_snake_case)]
struct KartJson {
  name: String,
  engineForce: f64,
  density: f64,
  turningSpeed: f64,
  driftingSpeed: f64,
  width: f64,
  length: f64,
  drawWidth: f64,
  drawHeight: f64,
}

pub struct Kart {
  pub name: String,
  file_name: String,

  turning_speed: f64,
  drifting_speed: f64,
  engine_force: f64,

  width: f64,
  length: f64,
  draw_width: f64,
  draw_height: f64,

  density: f64,

  pub transform: KartTransform,

  pub texture: SrgbTextureRegion,
}

fn angle_to_frame(angle: f64, range: u32) -> u32 {
  ((angle*(range as f64/(2.0*3.141592)).round()) as u32 % range + range) % range
}

impl Kart {
  pub fn new<F: glium::backend::Facade>(facade: &F, file_name: &str, spawn_point: KartTransform ) -> Result<Kart, String> {
    let mut kart_json = String::new();
    try!(File::open(format!("assets/karts/{}/kart.json", file_name)).unwrap().read_to_string(&mut kart_json).map_err(|_| "Couldn't open file!")); //TODO: do the resource management thing better
    if let Ok(kart_json) = from_str::<KartJson>(&kart_json) {
      return Ok(Kart {
        name: kart_json.name,
        file_name: file_name.to_string(),

        turning_speed: kart_json.turningSpeed,
        drifting_speed: kart_json.driftingSpeed,
        engine_force: kart_json.engineForce,

        width: kart_json.width,
        length: kart_json.length,
        density: kart_json.density,

        draw_width: kart_json.drawWidth,
        transform: spawn_point,

        texture: SrgbTextureRegion {
          texture: Rc::new(try!(load_srgb_texture(facade, format!("assets/karts/{}/sprites.png", file_name)).map_err(|_| format!("Couldn't load sprites for kart {}!", file_name)))),
          region: ((0.0, 0.0), (1.0/12.0, 1.0)),
        }
      });
    } else {
      return Err(format!("Invalid json in file {}!", file_name));
    }
  }

  pub fn get_decal(&self, rotz: f32, rotx: f32) -> Decal {
    Decal {
      transform: Transform {
        position: Vector3::new(self.transform.position.x as f32, self.transform.position.y as f32, 0.0),
        center: Vector3::new(0.0, -0.5, 0.0f32),
        scale: Vector3::new(self.draw_width as f32, self.draw_height as f32, 1.0f32),
        rotation: UnitQuaternion::new(Vector3::new(0.0, 0.0, -rotz))
          .append_rotation(&Vector3::new(-rotx, 0.0, 0.0f32)),
      },
      texture: self.texture.texture.clone(),
      tex_coords: [[self.texture.region.0 .0, self.texture.region.0 .1],
                   [self.texture.region.1 .0, self.texture.region.0 .1],
                   [self.texture.region.0 .0, self.texture.region.1 .1],
                   [self.texture.region.1 .0, self.texture.region.1 .1]],
    }
  }
} 
    
