use game::{
  GameController,
  InputProcessor,
};
use glium;
use glium::glutin::{
  VirtualKeyCode,
  ElementState,
};

pub struct DumbAIGameController;

impl GameController for DumbAIGameController {
  fn get_turning(&self) -> f64 { 1.0 }
  fn get_accelerator(&self) -> f64 { 1.0 }
  fn get_braking(&self) -> f64 { 0.0 }
  fn get_drifting(&self) -> bool { false }
}

pub struct KeyboardGameController {
  turning: f64,
  accelerator: (f64,f64),
  braking: f64,
  drifting: bool,
}

impl KeyboardGameController {
  pub fn new() -> KeyboardGameController {
    KeyboardGameController {
      turning: 0.0,
      accelerator: (0.0, 0.0),
      braking: 0.0,
      drifting: false,
    }
  }
}

impl GameController for KeyboardGameController {
  fn get_turning(&self) -> f64 { self.turning }
  fn get_accelerator(&self) -> f64 { self.accelerator.0 + self.accelerator.1 }
  fn get_braking(&self) -> f64 { self.braking }
  fn get_drifting(&self) -> bool { self.drifting }
}

impl InputProcessor for KeyboardGameController {
  fn input_event(&mut self, event: glium::glutin::Event) {
    match event {
      glium::glutin::Event::KeyboardInput(state, scancode, keycode) => {
        if let Some(keycode) = keycode {
          //self.keyboard_map.insert(keycode, state == glium::glutin::ElementState::Pressed);
          match keycode {
            VirtualKeyCode::W => {
              if state == ElementState::Pressed { self.accelerator.0 = 1.0 }
              else { self.accelerator.0 = 0.0 }
            }
            VirtualKeyCode::S => {
              if state == ElementState::Pressed { self.accelerator.1 = -1.0 }
              else { self.accelerator.1 = 0.0 }
            }
            _ => ()
          }
        }
      },
      _ => ()
    }
  }
}
