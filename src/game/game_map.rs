use std::fs;
use std::io::Read;
use glium;
use tiled;
//use image;
use render;
use nalgebra::{
  Point2,
};
use serde_json::{
  from_str,
};

pub struct Map {
  pub name: String,
  pub spawn_point: Point2<f64>,
  pub spawn_angle: f64,
  pub scale: f64, //pixels per meter
  //pub ground: glium::texture::RawImage2d<'a, u8> ,
  pub background: Background,
  //map:  
}

#[derive(Serialize, Deserialize)]
struct SpawnPoint {
  pub x: f64,
  pub y: f64,
  pub angle: f64,
}
#[derive(Serialize, Deserialize)]
pub struct Background {
  pub file: String,
  pub repetition: f64,
}
#[derive(Serialize, Deserialize)]
struct MapJson {
  name: String,
  spawn: SpawnPoint,
  scale: f64,
  ground: String,
  background: Background,
  map: String,
}

impl<'a, 'b> Map {
  pub fn new(name: &str) -> Result<(Map, glium::texture::RawImage2d<'a, u8>, glium::texture::RawImage2d<'a, u8>) , String> { //TODO: Use filenames dynamically or get them from a resource manager or something!
    println!("DEBUG: loading map {}", name);
    let mut map_json = String::new();
    fs::File::open(format!("assets/maps/{}/map.json", name)).unwrap().read_to_string(&mut map_json).unwrap();
    match from_str::<MapJson>(&map_json) {
      Ok(map_json) => {
        let ground = try!(render::load_raw_image(format!("assets/maps/{}/ground.png", name)).map_err(|_| "Couldn't load ground texture!".to_string()));
        let background = try!(render::load_raw_image(format!("assets/maps/{}/background.png", name)).map_err(|_| "Couldn't load background texture!".to_string()));

        return Ok((Map {
          name: map_json.name,
          spawn_point: Point2::new(map_json.spawn.x, map_json.spawn.y),
          spawn_angle: map_json.spawn.angle,
          scale: map_json.scale,
          background: map_json.background,
        },
        ground,
        background,));
      },
      Err(_) => {
        return Err("invalid data in map.json!".to_string());
      }
    }
  }
}
