use std::rc::Rc;
use std::io::Read;
use std::fs;
use glium;
use glium::{
  Surface,
};
use nalgebra;
use nalgebra::{
  Vector2, Vector3, Point3, Rotation3, UnitQuaternion, 
  Rotation,
  Rotate,
  ToHomogeneous,
  Translate,
};
use std::collections::HashMap;
use num_traits::One;
use math::{
  Clamp,
  Lerp,
};
use game::{
  Map,
  Kart,
  KartTransform,
  Screen,
  InputProcessor,
};
use render;
use render::{
  Renderer,
  CubemapVertex,
  Vertex2d,
  Decal,
  CameraTransform,
};

struct Framebuffer {
  resolution: (u32, u32),
  vertex_buffer: glium::VertexBuffer<Vertex2d>,
  program: glium::Program,
  texture: glium::texture::Texture2d,
  depth_texture: glium::texture::DepthTexture2d,
  //fbo: glium::framebuffer::SimpleFrameBuffer<'a>,
}
impl<'a> Framebuffer {
  fn new<F: glium::backend::Facade>(facade: &F, resolution: (u32,u32)) -> Result<Framebuffer, ()> {
    let vertex_buffer = try!(glium::VertexBuffer::new(facade, &vec![ Vertex2d { position: [-1.0,-1.0], tex_coords: [0.0,0.0] },
                                                                     Vertex2d { position: [ 1.0,-1.0], tex_coords: [1.0,0.0] },
                                                                     Vertex2d { position: [ 1.0, 1.0], tex_coords: [1.0,1.0] },
                                                                     Vertex2d { position: [-1.0, 1.0], tex_coords: [0.0,1.0] }]).map_err(|_|()));
    let texture = try!(glium::texture::Texture2d::empty(facade, resolution.0, resolution.1).map_err(|_|()));                  //TODO: (this entire function) better error handling
    let depth_texture = try!(glium::texture::DepthTexture2d::empty(facade, resolution.0, resolution.1).map_err(|_|()));
    Ok(Framebuffer {
      resolution: resolution,
      vertex_buffer: vertex_buffer,
      program: {
        let mut vertex_shader = String::new();
        fs::File::open("assets/shaders/gamescreen_postproc_150.vert").unwrap().read_to_string(&mut vertex_shader).unwrap();
        let mut fragment_shader = String::new();
        fs::File::open("assets/shaders/gamescreen_postproc_150.frag").unwrap().read_to_string(&mut fragment_shader).unwrap();
        glium::Program::from_source(facade, &vertex_shader, &fragment_shader, None).unwrap()
      },
      texture: texture,
      depth_texture: depth_texture,
      //fbo: fbo,
    })
  }
  fn resize<F: glium::backend::Facade>(&mut self, facade: &F, resolution: (u32, u32)) -> Result<(), ()> {
    if self.resolution != resolution {
      self.resolution = resolution;
      self.texture = try!(glium::texture::Texture2d::empty(facade, resolution.0, resolution.1).map_err(|_|()));                  //TODO: (this entire function) better error handling
      self.depth_texture = try!(glium::texture::DepthTexture2d::empty(facade, resolution.0, resolution.1).map_err(|_|()));
      //self.fbo = try!(glium::framebuffer::SimpleFrameBuffer::with_depth_buffer(facade, self.texture, self.depth_texture).map_err(|_|()));
    }
    Ok(())
  }
}

pub struct GameScreen<'facade, F: 'facade> where F: glium::backend::Facade {
  facade: &'facade F,

  program: glium::Program,
  skybox_program: glium::Program,

  camera: CameraTransform,

  decals: Vec<Decal>,
  karts: Vec<Kart>,

  state_time: f64,
  keyboard_map: HashMap<glium::glutin::VirtualKeyCode, bool>,

  framebuffer: Framebuffer,

  map_name: String,
  game_map: (Map, Rc<glium::texture::SrgbTexture2d>, Rc<glium::texture::SrgbTexture2d>),
  skybox_texture: glium::texture::SrgbCubemap,

  x: f32,
  y: f32,
  z: f32,
  rotx: f32,
  rotz: f32,
}

impl<'facade, F: glium::backend::Facade> GameScreen<'facade, F> {
  pub fn new(facade: &'facade F, map_name: String) -> GameScreen<F> {
    println!("DEBUG: creating GameScreen");
    let (game_map, ground, background) = Map::new(&map_name).unwrap();

    let mut screen = GameScreen {
      facade: facade,

      program: {
        let mut vertex_shader = String::new();
        fs::File::open("assets/shaders/game_screen_150.vert").unwrap().read_to_string(&mut vertex_shader).unwrap();
        let mut fragment_shader = String::new();
        fs::File::open("assets/shaders/game_screen_150.frag").unwrap().read_to_string(&mut fragment_shader).unwrap();
        glium::Program::from_source(facade, &vertex_shader, &fragment_shader, None).unwrap()
      },
      skybox_program: {
        let mut vertex_shader = String::new();
        fs::File::open("assets/shaders/skybox_150.vert").unwrap().read_to_string(&mut vertex_shader).unwrap();
        let mut fragment_shader = String::new();
        fs::File::open("assets/shaders/skybox_150.frag").unwrap().read_to_string(&mut fragment_shader).unwrap();
        glium::Program::from_source(facade, &vertex_shader, &fragment_shader, None).unwrap()
      },

      decals: Vec::new(),
      karts: Vec::new(),

      camera: CameraTransform::new(Vector3::new(0.0,0.0,0.0), UnitQuaternion::one(), 3.141592/4.0),

      state_time: 0.0,
      keyboard_map: HashMap::new(),

      framebuffer: Framebuffer::new(facade, (1,1)).unwrap(),
      x: game_map.spawn_point.x as f32,
      y: game_map.spawn_point.y as f32,

      map_name: map_name,
      game_map: (game_map, Rc::new(glium::texture::SrgbTexture2d::new(facade, ground).unwrap()), Rc::new(glium::texture::SrgbTexture2d::new(facade, background).unwrap())),
      skybox_texture: render::load_srgb_cubemap(facade, 
                                               "assets/skyboxes/smb3-skyhills/up.png",
                                               "assets/skyboxes/smb3-skyhills/down.png",
                                               "assets/skyboxes/smb3-skyhills/front.png",
                                               "assets/skyboxes/smb3-skyhills/back.png",
                                               "assets/skyboxes/smb3-skyhills/left.png",
                                               "assets/skyboxes/smb3-skyhills/right.png").unwrap(),
        
      z: 1.0,
      rotx: -1.5707,
      rotz: 0.0,
    };
    let ground_scale = Vector3::new(screen.game_map.1.get_width() as f32/screen.game_map.0.scale as f32, screen.game_map.1.get_height().unwrap() as f32/screen.game_map.0.scale as f32, 1.0f32);

    screen.decals.push(Decal::new(screen.game_map.1.clone()));
    screen.decals[0].transform.scale = ground_scale;

    screen.karts.push(Kart::new(screen.facade, "mario", KartTransform { position: screen.game_map.0.spawn_point, rotation: screen.game_map.0.spawn_angle, velocity: Vector2::new(0.0, 0.0) }).unwrap());

    screen
  }

  fn update_camera(&mut self, delta_time: f64) {
    let fov_intensifier: f32 = 0.0; //TODO
    let camera_angle: f64 = self.karts[0].transform.rotation;

    let aspect_ratio = self.framebuffer.resolution.1 as f32 / self.framebuffer.resolution.0 as f32;

    let (v_fov, h_fov) = {
      let v_fov = 3.141592/4.0;
      let h_fov = 2.0 * ((v_fov/2.0f32).tan() / aspect_ratio).atan();
      (v_fov, h_fov)
    };

    let camera_position = Point3::new(-8.0, 0.0, 4.0f64);
    let camera_position = Rotation3::new(Vector3::new(0.0, 0.0, camera_angle)).rotate(&camera_position);
    let camera_position = self.karts[0].transform.position.to_homogeneous().as_vector().translate(&camera_position);

    self.camera = CameraTransform::new(
      nalgebra::cast::<Vector3<f64>,Vector3<f32>>(camera_position.to_vector()),
      UnitQuaternion::new(Vector3::new(self.rotx, 0.0, 0.0))
        .append_rotation(&Vector3::new(0.0, 0.0, (-camera_angle-3.141592/2.0) as f32)),
      0.1f32.lerp(self.camera.fov, v_fov+fov_intensifier)
    );

  }

  fn render_onto_target<'a, T: glium::Surface>(&'a self, target: &mut T) {
    let (width, height) = target.get_dimensions();
    let aspect_ratio = height as f32 / width as f32;

    
    /*self.camera = CameraTransform::new(
      Vector3::new(self.x, self.y, self.z),
      UnitQuaternion::new(Vector3::new(self.rotx, 0.0, 0.0))
        .append_rotation(&Vector3::new(0.0, 0.0, self.rotz)),
      v_fov
    );*/
    
    let projection = render::perspective_matrix(aspect_ratio, self.camera.fov, 1024.0, 0.1);
    target.clear_color_and_depth((0.0,0.0,0.0,1.0),1.0);

    let skybox_vertices = vec![
        //BOTTOM FACE
        CubemapVertex { position:[-1.0,-1.0,-1.0] },
        CubemapVertex { position:[ 1.0,-1.0,-1.0] },
        CubemapVertex { position:[-1.0, 1.0,-1.0] },
        CubemapVertex { position:[ 1.0, 1.0,-1.0] },
        
        //TOP FACE
        CubemapVertex { position:[-1.0,-1.0, 1.0] },
        CubemapVertex { position:[ 1.0,-1.0, 1.0] },
        CubemapVertex { position:[-1.0, 1.0, 1.0] },
        CubemapVertex { position:[ 1.0, 1.0, 1.0] },
        
        //FRONT FACE
        CubemapVertex { position:[-1.0, 1.0,-1.0] },
        CubemapVertex { position:[ 1.0, 1.0,-1.0] },
        CubemapVertex { position:[-1.0, 1.0, 1.0] },
        CubemapVertex { position:[ 1.0, 1.0, 1.0] },
        
        //BACK FACE
        CubemapVertex { position:[-1.0,-1.0,-1.0] },
        CubemapVertex { position:[ 1.0,-1.0,-1.0] },
        CubemapVertex { position:[-1.0,-1.0, 1.0] },
        CubemapVertex { position:[ 1.0,-1.0, 1.0] },
        
        //LEFT FACE
        CubemapVertex { position:[ 1.0,-1.0,-1.0] },
        CubemapVertex { position:[ 1.0, 1.0,-1.0] },
        CubemapVertex { position:[ 1.0,-1.0, 1.0] },
        CubemapVertex { position:[ 1.0, 1.0, 1.0] },
        
        //RIGHT FACE
        CubemapVertex { position:[-1.0,-1.0,-1.0] },
        CubemapVertex { position:[-1.0, 1.0,-1.0] },
        CubemapVertex { position:[-1.0,-1.0, 1.0] },
        CubemapVertex { position:[-1.0, 1.0, 1.0] },
      ];
    let skybox_triangles = vec![
         0, 1, 2, 1, 3, 2, //bottom
         4, 6, 5, 5, 6, 7, //top
         8, 9,10, 9,11,10, //front
        12,14,13,13,14,15, //back
        16,18,17,17,18,19, //left
        20,21,22,21,23,22u32, //right
      ];
    let background_vertex_buffer = glium::VertexBuffer::new(self.facade, &skybox_vertices).unwrap();
    let skybox_index_buffer = glium::IndexBuffer::new(self.facade, glium::index::PrimitiveType::TrianglesList, &skybox_triangles).unwrap();

    let mut renderer = Renderer::new();

    for r in &self.decals {
      renderer.draw(r.get_renderable());
    }

    let camera_kart_rotation = -self.karts[0].transform.rotation as f32 - 3.141592/2.0;
    for k in &self.karts {
      renderer.draw(k.get_decal(camera_kart_rotation, self.rotx).get_renderable());
    }

    renderer.flush(
      self.facade,
      target,
      &self.program,
      &self.camera,
      &projection
    );
    
    target.draw(&background_vertex_buffer, &skybox_index_buffer, &self.skybox_program,
      &uniform!{
        cubemap: self.skybox_texture.sampled()
          .magnify_filter(glium::uniforms::MagnifySamplerFilter::Linear)
          .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
          .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp),

        perspective: {
          let p = &projection;
          [
            [p.m11, p.m21, p.m31, p.m41],
            [p.m12, p.m22, p.m32, p.m42],
            [p.m13, p.m23, p.m33, p.m43],
            [p.m14, p.m24, p.m34, p.m44],
          ]
        },

        view: {
          let p = &self.camera.get_camera_matrix();
          [
            [p.m11, p.m21, p.m31, p.m41],
            [p.m12, p.m22, p.m32, p.m42],
            [p.m13, p.m23, p.m33, p.m43],
            [0.0, 0.0, 0.0, /*p.m14, p.m24, p.m34,*/ p.m44],
          ]
        },
      },
      &glium::DrawParameters{
        backface_culling: glium::BackfaceCullingMode::CullCounterClockwise,
        depth: glium::Depth {
          test: glium::draw_parameters::DepthTest::IfLessOrEqual,
          write: true,
          .. Default::default()
        },
        .. Default::default()
      }
    ).unwrap();
  }
}

impl<'facade, F: glium::backend::Facade> InputProcessor for GameScreen<'facade, F> {
  fn input_event(&mut self, event: glium::glutin::Event) {
    match event {
      glium::glutin::Event::KeyboardInput(state, scancode, keycode) => {
        if let Some(keycode) = keycode {
          self.keyboard_map.insert(keycode, state == glium::glutin::ElementState::Pressed);
        }
      },
      _ => ()
    }
  }
}

impl<'facade, F: glium::backend::Facade> Screen for GameScreen<'facade, F> {

  fn update(&mut self, delta_time: f64) {
    self.state_time += delta_time;

    self.decals[0].transform.rotation = UnitQuaternion::new_with_euler_angles(3.1415,0.0,0.0f32);
 
    //We just have free camera movement for now, karts come later
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::D).unwrap_or(&false) {
      self.y -= self.rotz.sin() * delta_time as f32 * 7.0;
      self.x += self.rotz.cos() * delta_time as f32 * 7.0;
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::A).unwrap_or(&false) {
      self.y += self.rotz.sin() * delta_time as f32 * 7.0;
      self.x -= self.rotz.cos() * delta_time as f32 * 7.0;
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::W).unwrap_or(&false) {
      self.y -= self.rotz.cos() * delta_time as f32 * 14.0;
      self.x -= self.rotz.sin() * delta_time as f32 * 14.0;
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::S).unwrap_or(&false) {
      self.y += self.rotz.cos() * delta_time as f32 * 14.0;
      self.x += self.rotz.sin() * delta_time as f32 * 14.0;
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::R).unwrap_or(&false) {
      self.z += delta_time as f32;
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::F).unwrap_or(&false) {
      self.z -= delta_time as f32;
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::Up).unwrap_or(&false) {
      self.rotx = nalgebra::clamp(self.rotx + delta_time as f32, -3.141592, 0.0);
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::Down).unwrap_or(&false) {
      self.rotx = nalgebra::clamp(self.rotx - delta_time as f32, -3.141592, 0.0);
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::Left).unwrap_or(&false) {
      self.karts[0].transform.rotation += delta_time;
    }
    if *self.keyboard_map.get(&glium::glutin::VirtualKeyCode::Right).unwrap_or(&false) {
      self.karts[0].transform.rotation -= delta_time;
    }

    self.update_camera(delta_time);

  }

  fn render<'a>(&'a mut self, target: &mut glium::Frame) {
    let resolution = target.get_dimensions();
    if resolution != self.framebuffer.resolution {
      self.framebuffer.resize(self.facade, resolution).unwrap();
      println!("DEBUG: resolution changed");
    }
    let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::with_depth_buffer(self.facade, &self.framebuffer.texture, &self.framebuffer.depth_texture).unwrap();

    self.render_onto_target(&mut framebuffer);

    target.draw(&self.framebuffer.vertex_buffer, &glium::index::NoIndices(glium::index::PrimitiveType::TriangleFan), &self.framebuffer.program,
                &uniform!{
                  tex: self.framebuffer.texture.sampled(),
                  resolution: [self.framebuffer.resolution.0 as f32, self.framebuffer.resolution.1 as f32],
                },
                &glium::DrawParameters {
                  .. Default::default()
                }).unwrap();
  }
}
