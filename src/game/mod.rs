use glium;

mod game_screen;
mod kart;
mod game_map;
mod game_controller;

pub use game::game_screen::GameScreen;
pub use game::kart::{
  Kart,
  KartTransform,
};
pub use game::game_map::Map;

pub trait Screen {
  fn update(&mut self, delta_time: f64);
  // glium::Frame is the solution I ended up with, because trait objects can't
  // have generic functions, so I can't use glium::Surface. This is probably okay.
  fn render<'b>(&'b mut self, target: &mut glium::Frame);
}

pub enum Screens {
  GameScreen,
}

pub use game::game_controller::*;
pub trait GameController {
  fn get_turning(&self) -> f64;
  fn get_accelerator(&self) -> f64;
  fn get_braking(&self) -> f64;
  fn get_drifting(&self) -> bool;
}

pub trait InputProcessor {
  fn input_event(&mut self, event: glium::glutin::Event);
}
