//TODO: THIS MODULE IS A HACK THAT LOADS A CUBEMAP TEXTURE FOR THE SKYBOX.
//Something better should be done once glium has a good way to do this
use glium;
use glium::Surface;
use render::LoadError;

pub fn new_with_textures<F: glium::backend::Facade>(
  facade: &F,
  up: glium::texture::Texture2d,
  down: glium::texture::Texture2d,
  front: glium::texture::Texture2d,
  back: glium::texture::Texture2d,
  left: glium::texture::Texture2d,
  right: glium::texture::Texture2d) -> Result<glium::texture::SrgbCubemap, LoadError> {

  println!("DEBUG: creating cubemap texture");

  if !{
    let image_dimensions = [
      up.get_width(), up.get_height().unwrap(),
      down.get_width(), down.get_height().unwrap(),
      front.get_width(), front.get_height().unwrap(),
      back.get_width(), back.get_height().unwrap(),
      left.get_width(), left.get_height().unwrap(),
      right.get_width(), right.get_height().unwrap()
    ];
    image_dimensions.iter().all(|&x| x == image_dimensions[0])
  } { return Err(LoadError::InvalidSize(Some("All textures have to be square and equal size!".to_string()))); }

  let cubemap = try!(glium::texture::SrgbCubemap::empty_with_format(facade,
                                                               glium::texture::SrgbFormat::U8U8U8U8,
                                                               glium::texture::MipmapsOption::NoMipmap,
                                                               up.get_width()).map_err(|e| LoadError::TextureCreationError(e)));
  {
    let target = glium::framebuffer::SimpleFrameBuffer::new(facade, cubemap.main_level().image(glium::texture::CubeLayer::PositiveZ)).unwrap();
    up.as_surface().fill(&target, glium::uniforms::MagnifySamplerFilter::Linear);
  }

  {
    let target = glium::framebuffer::SimpleFrameBuffer::new(facade, cubemap.main_level().image(glium::texture::CubeLayer::NegativeZ)).unwrap();
    down.as_surface().fill(&target, glium::uniforms::MagnifySamplerFilter::Linear);
  }

  {
    let target = glium::framebuffer::SimpleFrameBuffer::new(facade, cubemap.main_level().image(glium::texture::CubeLayer::PositiveY)).unwrap();
    front.as_surface().fill(&target, glium::uniforms::MagnifySamplerFilter::Linear);
  }

  {
    let target = glium::framebuffer::SimpleFrameBuffer::new(facade, cubemap.main_level().image(glium::texture::CubeLayer::NegativeY)).unwrap();
    back.as_surface().fill(&target, glium::uniforms::MagnifySamplerFilter::Linear);
  }

  {
    let target = glium::framebuffer::SimpleFrameBuffer::new(facade, cubemap.main_level().image(glium::texture::CubeLayer::PositiveX)).unwrap();
    right.as_surface().fill(&target, glium::uniforms::MagnifySamplerFilter::Linear);
  }

  {
    let target = glium::framebuffer::SimpleFrameBuffer::new(facade, cubemap.main_level().image(glium::texture::CubeLayer::NegativeX)).unwrap();
    left.as_surface().fill(&target, glium::uniforms::MagnifySamplerFilter::Linear);
  }

  Ok(cubemap)
}
