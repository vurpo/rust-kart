use std::rc::Rc;
use glium;
use nalgebra::{
  Vector3,
};
use render::{
  Renderable, Transform, Vertex
};

pub struct Decal {
  pub transform: Transform,
  pub texture: Rc<glium::texture::SrgbTexture2d>,
  pub tex_coords: [[f32;2];4]
}

impl Decal {
  pub fn new(texture: Rc<glium::texture::SrgbTexture2d>) -> Decal {
    Decal {
      transform: Transform::new(Vector3::new(0.0,0.0,0.0f32), Vector3::new(0.0,0.0,0.0f32), Vector3::new(1.0,1.0,1.0f32)),
      texture: texture,
      tex_coords: [
        [0.0, 0.0],
        [1.0, 0.0],
        [0.0, 1.0],
        [1.0, 1.0],
      ],
    }
  }

  pub fn get_renderable(&self) -> Renderable {
    Renderable {
      vertices: vec![
        Vertex { position: [-0.5, -0.5, 0.0], tex_coords: self.tex_coords[0]},
        Vertex { position: [ 0.5, -0.5, 0.0], tex_coords: self.tex_coords[1] },
        Vertex { position: [-0.5,  0.5, 0.0], tex_coords: self.tex_coords[2] },
        Vertex { position: [ 0.5,  0.5, 0.0], tex_coords: self.tex_coords[3] },
      ],
      triangles: vec![ 0,1,3,0,3,2 ],
      transform: self.transform.get_matrix().clone(),
      texture: self.texture.clone(),
    }
  }   
}

