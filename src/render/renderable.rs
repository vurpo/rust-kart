use std::rc::Rc;
use glium;
use nalgebra::{
  Matrix4
};
use render::CameraTransform;

use render::Vertex;

pub struct Renderable {
  pub vertices: Vec<Vertex>,
  pub triangles: Vec<u16>, //TODO:only supports triangles list for now, maybe use some sort of enum
  pub transform: Matrix4<f32>,
  pub texture: Rc<glium::texture::SrgbTexture2d>,
}

pub struct Renderer {
  renderables: Vec<Renderable>,
}

impl Renderer {
  pub fn new() -> Renderer {
    Renderer {
      renderables: Vec::new(),
    }
  }

  pub fn draw(&mut self, renderable: Renderable) {
    self.renderables.push(renderable);
}
  
  pub fn flush<S: glium::Surface, F: glium::backend::Facade>
          (&mut self,
           facade: &F,
           surface: &mut S,
           program: &glium::Program,
           camera: &CameraTransform,
           projection: &Matrix4<f32>
           ) {
    for r in self.renderables.drain(..) {
      let uniforms = uniform! {
        tex: r.texture.sampled()
          .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
          .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
          .wrap_function(glium::uniforms::SamplerWrapFunction::Repeat),

        perspective: {
          let p = &projection;
          [
            [p.m11, p.m21, p.m31, p.m41],
            [p.m12, p.m22, p.m32, p.m42],
            [p.m13, p.m23, p.m33, p.m43],
            [p.m14, p.m24, p.m34, p.m44],
          ]
        },

        view: {
          let p = &camera.get_camera_matrix();
          [
            [p.m11, p.m21, p.m31, p.m41],
            [p.m12, p.m22, p.m32, p.m42],
            [p.m13, p.m23, p.m33, p.m43],
            [p.m14, p.m24, p.m34, p.m44],
          ]
        },

        model: {
          let p = r.transform;
          [
            [p.m11, p.m21, p.m31, p.m41],
            [p.m12, p.m22, p.m32, p.m42],
            [p.m13, p.m23, p.m33, p.m43],
            [p.m14, p.m24, p.m34, p.m44],
          ]
        },
      };

      let vertex_buffer = glium::VertexBuffer::new(facade, &r.vertices).unwrap();
      
      let indices = glium::IndexBuffer::new(facade, glium::index::PrimitiveType::TrianglesList, &r.triangles).unwrap();
      surface.draw(&vertex_buffer, &indices, program, &uniforms,
        &glium::DrawParameters{
          depth: glium::Depth {
            test: glium::draw_parameters::DepthTest::IfLess,
            write: true,
            .. Default::default()
          },
          blend: glium::Blend::alpha_blending(),
          backface_culling: glium::BackfaceCullingMode::CullingDisabled,
          .. Default::default()
        }).unwrap();
    }
  }
}
