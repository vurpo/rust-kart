use std;
use std::rc::Rc;
use image;
use glium;
use render::cubemaphack;

#[derive(Debug)]
pub enum LoadError {
  ImageError(image::ImageError),
  TextureCreationError(glium::texture::TextureCreationError),
  InvalidSize(Option<String>),
}

pub fn load_raw_image<'a, P: AsRef<std::path::Path>>(path: P) -> Result<glium::texture::RawImage2d<'a, u8>, LoadError> {
  let raw_image = try!(image::open(path).map_err(|e| LoadError::ImageError(e))).to_rgba();
  let image_dimensions = raw_image.dimensions();
  return Ok(glium::texture::RawImage2d::from_raw_rgba_reversed(raw_image.into_raw(), image_dimensions));
}

pub fn load_texture<F: glium::backend::Facade, P: AsRef<std::path::Path>>(facade: &F, path: P) -> Result<glium::texture::Texture2d, LoadError> {
  println!("DEBUG: loading image {} to Texture2d", path.as_ref().to_str().unwrap());
  let raw_image = try!(load_raw_image(path));
  return glium::texture::Texture2d::new(facade, raw_image).map_err(|e| LoadError::TextureCreationError(e));
}

pub fn load_srgb_texture<F: glium::backend::Facade, P: AsRef<std::path::Path>>(facade: &F, path: P) -> Result<glium::texture::SrgbTexture2d, LoadError> {
  println!("DEBUG: loading image {} to SrgbTexture2d", path.as_ref().to_str().unwrap());
  let raw_image = try!(load_raw_image(path));
  match glium::texture::SrgbTexture2d::new(facade, raw_image) {
    Ok(texture) => { return Ok(texture); }
    Err(e) => { return Err(LoadError::TextureCreationError(e)); }
  }
}

pub fn load_srgb_cubemap<F: glium::backend::Facade, P: AsRef<std::path::Path>>(facade: &F, up: P, down: P, front: P, back: P, left: P, right: P) -> Result<glium::texture::SrgbCubemap, LoadError> {
  println!("DEBUG: loading SrgbCubemap");
  let up = try!(load_texture(facade, up));
  let down = try!(load_texture(facade, down));
  let front = try!(load_texture(facade, front));
  let back = try!(load_texture(facade, back));
  let left = try!(load_texture(facade, left));
  let right = try!(load_texture(facade, right));

  cubemaphack::new_with_textures(facade, up, down, front, back, left, right)
}

pub struct SrgbTextureRegion {
  pub texture: Rc<glium::texture::SrgbTexture2d>,
  pub region: ((f32,f32),(f32,f32)),
}

pub struct TextureRegion {
  pub texture: Rc<glium::texture::Texture2d>,
  pub region: ((f32,f32),(f32,f32)),
}
