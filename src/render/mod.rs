use nalgebra::{
  Matrix4,
};

mod transform;
mod renderable;
mod decal;
pub mod cubemaphack;
mod texture;

pub use render::transform::Transform;
pub use render::transform::CameraTransform;
pub use render::renderable::Renderable;
pub use render::renderable::Renderer;
pub use render::decal::Decal;
pub use render::texture::{
  LoadError,
  load_raw_image,
  load_texture,
  load_srgb_texture,
  load_srgb_cubemap,
  SrgbTextureRegion,
  TextureRegion,
};

#[derive(Copy, Clone, Debug)]
pub struct Vertex {
  pub position: [f32; 3],
  pub tex_coords: [f32; 2],
}

implement_vertex!(Vertex, position, tex_coords);

#[derive(Copy, Clone, Debug)]
pub struct CubemapVertex {
  pub position: [f32; 3],
}

implement_vertex!(CubemapVertex, position);

#[derive(Copy, Clone, Debug)]
pub struct Vertex2d {
  pub position: [f32; 2],
  pub tex_coords: [f32; 2],
}

implement_vertex!(Vertex2d, position, tex_coords);

/*pub fn view_matrix(position: &Point3<f32>, direction: &Vector3<f32>, up: &Vector3<f32>) -> Matrix4<f32> {
  let f = direction.normalize();

  let s = Vector3::new(
    up.y * f.z - up.z * f.y,
    up.z * f.x - up.x * f.z,
    up.x * f.y - up.y * f.x);

  let s_norm = s.normalize();

  let u = Vector3::new(
    f.y * s_norm.z - f.z * s_norm.y,
    f.z * s_norm.x - f.x * s_norm.z,
    f.x * s_norm.y - f.y * s_norm.x);

  let p = Vector3::new(
    -position.x * s_norm.x - position.y * s_norm.y - position.z * s_norm.z,
    -position.x * u.x - position.y * u.y - position.z * u.z,
    -position.x * f.x - position.y * f.y - position.z * f.z);

  Matrix4::from([
    [s.x, u.x, f.x, 0.0],
    [s.y, u.y, f.y, 0.0],
    [s.z, u.z, f.z, 0.0],
    [p.x, p.y, p.z, 1.0],
  ])
}

pub fn look_at_matrix(position: &Vector3<f32>, target: &Vector3<f32>, up: &Vector3<f32>) -> Matrix4<f32> {
  let z_axis = (position - target).normalize();
  let x_axis = up.cross(z_axis).normalize();
  let y_axis = z_axis.cross(x_axis);

  let orientation: Matrix4<f32> = Matrix4 {
    x: Vector4::new(x_axis.x, y_axis.x, z_axis.x, 0.0),
    y: Vector4::new(x_axis.y, y_axis.y, z_axis.y, 0.0),
    z: Vector4::new(x_axis.z, y_axis.z, z_axis.z, 0.0),
    w: Vector4::new(0.0, 0.0, 0.0, 1.0),
  };

  let translation: Matrix4<f32> = Matrix4 {
    x: Vector4::new(1.0, 0.0, 0.0, 0.0),
    y: Vector4::new(0.0, 1.0, 0.0, 0.0),
    z: Vector4::new(0.0, 0.0, 1.0, 0.0),
    w: Vector4::new(-position.x, -position.y, -position.z, 1.0),
  };

  orientation * translation
}*/

pub fn perspective_matrix(aspect_ratio: f32, fov: f32, zfar: f32, znear: f32) -> Matrix4<f32> {  //FOV is vertical!
  let f = 1.0 / (fov / 2.0).tan();

  Matrix4::new(
    f *   aspect_ratio   ,    0.0,              0.0              ,   0.0,
             0.0         ,     f ,              0.0              ,   0.0,
             0.0         ,    0.0,  (zfar+znear)/(zfar-znear)    ,   -(2.0*zfar*znear)/(zfar-znear),
             0.0         ,    0.0,              1.0              ,   0.0f32,
  )
}
