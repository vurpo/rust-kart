use nalgebra::{
  Matrix4, Vector3, UnitQuaternion,
};

pub struct Transform {
  pub position: Vector3<f32>,
  pub center: Vector3<f32>,
  pub scale: Vector3<f32>,
  pub rotation: UnitQuaternion<f32>,

  
}

impl Transform {
  pub fn new(position: Vector3<f32>, center: Vector3<f32>, scale: Vector3<f32>) -> Transform {
    Transform {
      position: position,
      center: center,
      scale: scale,
      rotation: UnitQuaternion::new_with_euler_angles(0.0,0.0,0.0f32),
    }
  }
  
  pub fn get_matrix(&self) -> Matrix4<f32> {
    let center_translation_matrix = Matrix4::new(
      1.0, 0.0, 0.0, -self.center.x,
      0.0, 1.0, 0.0, -self.center.y,
      0.0, 0.0, 1.0, -self.center.z,
      0.0, 0.0, 0.0, 1.0f32,
    );
    let r = self.rotation.to_rotation_matrix();
    let r = r.submatrix();
    let rotation_matrix = Matrix4::new(
      r.m11, r.m12, r.m13, 0.0,
      r.m21, r.m22, r.m23, 0.0,
      r.m31, r.m32, r.m33, 0.0,
      0.0,   0.0,   0.0,   1.0,
    );
    let position_translation_matrix = Matrix4::new(
      1.0, 0.0, 0.0, self.position.x,
      0.0, 1.0, 0.0, self.position.y,
      0.0, 0.0, 1.0, self.position.z,
      0.0, 0.0, 0.0, 1.0f32,
    );

    let scale_matrix = Matrix4::new(
      self.scale.x, 0.0, 0.0, 0.0,
      0.0, self.scale.y, 0.0, 0.0,
      0.0, 0.0, self.scale.z, 0.0,
      0.0, 0.0, 0.0, 1.0f32,
    );
    
    position_translation_matrix * rotation_matrix * scale_matrix * center_translation_matrix //read right to left!
  } 
}

pub struct CameraTransform {
  pub position: Vector3<f32>,
  pub rotation: UnitQuaternion<f32>,
  pub scale: Vector3<f32>,
  pub fov: f32,
}

impl CameraTransform {
  pub fn new(position: Vector3<f32>, rotation: UnitQuaternion<f32>, fov: f32) -> CameraTransform {
    CameraTransform {
      position: position,
      rotation: rotation,
      scale: Vector3::new(1.0, 1.0, 1.0f32),
      fov: fov,
    }
  }
  pub fn get_camera_matrix(&self) -> Matrix4<f32> {

    let r = self.rotation.to_rotation_matrix();
    let r = r.submatrix();
    let rotation_matrix = Matrix4::new(
      r.m11, r.m12, r.m13, 0.0,
      r.m21, r.m22, r.m23, 0.0,
      r.m31, r.m32, r.m33, 0.0,
      0.0,   0.0,   0.0,   1.0,
    );
    let position_translation_matrix = Matrix4::new(
      1.0, 0.0, 0.0, -self.position.x,
      0.0, 1.0, 0.0, -self.position.y,
      0.0, 0.0, 1.0, -self.position.z,
      0.0, 0.0, 0.0, 1.0f32,
    );

    let scale_matrix = Matrix4::new(
      self.scale.x, 0.0, 0.0, 0.0,
      0.0, self.scale.y, 0.0, 0.0,
      0.0, 0.0, self.scale.z, 0.0,
      0.0, 0.0, 0.0, 1.0f32,
    );
    
    rotation_matrix * scale_matrix * position_translation_matrix //read right to left!
  }
}
