
pub trait Clamp<T: PartialOrd> {
  fn clamp(self, min: T, max: T) -> T;
}

impl<T: PartialOrd> Clamp<T> for T {
  fn clamp(self, min: T, max: T) -> T {
    if self.lt(&min) { min }
    else if self.gt(&max) { max }
    else { self }
  }
}

pub trait Lerp {
  fn lerp(&self, from: Self, to: Self) -> Self;
}

impl Lerp for f32 {
  fn lerp(&self, from: f32, to: f32) -> f32 {
    (to-from)*self.clamp(&0.0f32, &1.0f32) + from
  }
}


impl Lerp for f64 {
  fn lerp(&self, from: f64, to: f64) -> f64 {
    (to-from)*self.clamp(&0.0f64, &1.0f64) + from
  }
}
